import { Component, ElementRef, ViewChild } from '@angular/core';
import { RouterModule } from '@angular/router';




@Component({
  selector: 'app-header',
  standalone: true,
  imports: [RouterModule],
  templateUrl: './header.component.html',
  styleUrl: './header.component.css'
})
export class HeaderComponent {
  @ViewChild('userProfileCollapse')
  userProfileElement!: ElementRef; 
  

  toggleUserProfile() {
    this.userProfileElement.nativeElement.classList.toggle('show');
  }

}
