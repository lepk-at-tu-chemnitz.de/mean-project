import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { GoogleMapsModule } from '@angular/google-maps'

import { HeaderComponent } from "../home/header/header.component";
import { MapComponent } from "./map/map.component";
import { FooterComponent } from "./footer/footer.component";
import { RouterModule } from '@angular/router';


@Component({
    selector: 'app-home',
    standalone: true,
    templateUrl: './home.component.html',
    styleUrl: './home.component.css',
    imports: [
      FormsModule,
      CommonModule,
      HeaderComponent,
      MapComponent,
      FooterComponent,
      MatSlideToggleModule,
      GoogleMapsModule,
      RouterModule
    ],
})
export default class HomeComponent {

}
