import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { GoogleMapsModule, MapInfoWindow, MapMarker } from '@angular/google-maps';
import { environment } from './environment';
import { Jugendberufshilfens } from '../../shared/categories/jugendberufshilfens.model';
import { Schulsozialarbeit } from '../../shared/categories/schulsozialarbeit.model';
import { Kindertageseinrichtungen } from '../../shared/categories/kindertageseinrichtungen.model';
import { Schulen } from '../../shared/categories/schulen.model';
import { FavoritesService } from '../../shared/services/favorites.service';
import { AuthService } from '../../shared/services/auth.service';

@Component({
  selector: 'app-map',
  standalone: true,
  imports: [
    CommonModule,
    GoogleMapsModule
  ],
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {
  center = { lat: 50.81443474873337, lng: 12.9112975837362 };
  zoom = 12;
  facilities: any[] = [];
  schulsozialarbeitFacilities: Schulsozialarbeit[] = [];
  jugendberufshilfensFacilities: Jugendberufshilfens[] = [];
  kindertageseinrichtungenFacilities: Kindertageseinrichtungen[] = [];
  schulenFacilities: Schulen[] = [];
  apiLoaded: boolean = false;
  selectedFacilities: any[] = [];
  additionalInfo: any[] = [];
  showJugendMarkers: boolean = true;
  showSchulsozialarbeitMarkers: boolean = true;
  showKindertageseinrichtungenMarkers: boolean = true;
  showSchulenMarkers: boolean = true;
  showMoreInfo: boolean[] = [];
  showGiveAddress: boolean[] = [];
  userId: string | null = null;
  @ViewChild(MapInfoWindow) infoWindow: MapInfoWindow | undefined;
  showButtons = false;

  constructor(
    private http: HttpClient,
    private favoritesService: FavoritesService,
    private authService: AuthService
  ) {}

  ngOnInit(): void {
    this.loadGoogleMapsApi();
    this.loadFacilities();
    this.loadSchulsozialarbeitFacilities();
    this.loadKindertageseinrichtungenFacilities();
    this.loadSchulenFacilities();
    this.userId = this.authService.getCurrentUserId();
    console.log('Retrieved user ID:', this.userId);
    if (!this.userId) {
      console.error('User ID is not available');
    }
  }

  loadGoogleMapsApi(): void {
    const script = document.createElement('script');
    script.src = `https://maps.googleapis.com/maps/api/js?key=${environment.googleMapsApiKey}`;
    script.async = true;
    script.defer = true;
    script.onload = () => {
      this.apiLoaded = true;
    };
    document.head.appendChild(script);
  }

  loadFacilities(): void {
    this.http.get<Jugendberufshilfens[]>('http://localhost:3000/jugendberufshilfens')
      .subscribe((data: Jugendberufshilfens[]) => {
        this.jugendberufshilfensFacilities = data;
        this.updateFacilities();
        console.log('Jugendberufshilfens loaded:', this.jugendberufshilfensFacilities);
      });
  }

  loadSchulsozialarbeitFacilities(): void {
    this.http.get<Schulsozialarbeit[]>('http://localhost:3000/schulsozialarbeit')
      .subscribe((data: Schulsozialarbeit[]) => {
        this.schulsozialarbeitFacilities = data;
        this.updateFacilities();
        console.log('Schulsozialarbeit loaded:', this.schulsozialarbeitFacilities);
      });
  }

  loadKindertageseinrichtungenFacilities(): void {
    this.http.get<Kindertageseinrichtungen[]>('http://localhost:3000/kindertageseinrichtungen')
      .subscribe((data: Kindertageseinrichtungen[]) => {
        this.kindertageseinrichtungenFacilities = data;
        this.updateFacilities();
        console.log('Kindertageseinrichtungen loaded:', this.kindertageseinrichtungenFacilities);
      });
  }

  loadSchulenFacilities(): void {
    this.http.get<Schulen[]>('http://localhost:3000/schulen')
      .subscribe((data: Schulen[]) => {
        this.schulenFacilities = data;
        this.updateFacilities();
        console.log('Schulen loaded:', this.schulenFacilities);
      });
  }

  updateFacilities(): void {
    this.facilities = [
      ...this.showJugendMarkers ? this.jugendberufshilfensFacilities.map(f => ({ ...f, icon: 'http://maps.google.com/mapfiles/ms/icons/red-dot.png' })) : [],
      ...this.showSchulsozialarbeitMarkers ? this.schulsozialarbeitFacilities.map(f => ({ ...f, icon: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png' })) : [],
      ...this.showKindertageseinrichtungenMarkers ? this.kindertageseinrichtungenFacilities.map(f => ({ ...f, icon: 'http://maps.google.com/mapfiles/ms/icons/green-dot.png' })) : [],
      ...this.showSchulenMarkers ? this.schulenFacilities.map(f => ({ ...f, icon: 'http://maps.google.com/mapfiles/ms/icons/yellow-dot.png' })) : [],
    ];
  }

  selectFacility(facility: any, marker: MapMarker): void {
    this.selectedFacilities = this.facilities.filter(f => f.Y === facility.Y && f.X === facility.X);
    this.additionalInfo = new Array(this.selectedFacilities.length).fill(null);
    this.showMoreInfo = new Array(this.selectedFacilities.length).fill(false);
    this.showGiveAddress = new Array(this.selectedFacilities.length).fill(false);
    if (this.infoWindow) {
      this.infoWindow.open(marker);
    }
  }

  closeInfoWindow(): void {
    if (this.infoWindow) {
      this.infoWindow.close();
    }
    this.selectedFacilities = [];
    this.additionalInfo = [];
    this.showMoreInfo = [];
    this.showGiveAddress = [];
  }

  fetchAdditionalInfo(index: number, lat: number, lng: number): void {
    const geocodingUrl = `https://maps.googleapis.com/maps/api/geocode/json?latlng=${lat},${lng}&key=${environment.googleMapsApiKey}`;
    this.http.get(geocodingUrl).subscribe((response: any) => {
      if (response.results && response.results.length > 0) {
        this.additionalInfo[index] = response.results[0];
      }
    });
  }

  toggleMoreInfo(index: number): void {
    this.showMoreInfo[index] = !this.showMoreInfo[index];
  }

  toggleGiveAddress(index: number): void {
    this.showGiveAddress[index] = !this.showGiveAddress[index];
    if (this.showGiveAddress[index]) {
      const facility = this.selectedFacilities[index];
      this.fetchAdditionalInfo(index, facility.Y, facility.X);
    }
  }

  toggleFavorite(facility: any): void {
    if (this.userId) {
      this.favoritesService.addFavorite(this.userId, facility._id).subscribe({
        next: (response) => {
          console.log('Added to favorites', response);
        },
        error: (error) => {
          console.error('Error adding to favorites:', error);
        }
      });
    } else {
      console.error('User ID is not available');
    }
  }

  toggleMarkers(facilityType: string): void {
    if (facilityType === 'jugendberufshilfens') {
      this.showJugendMarkers = !this.showJugendMarkers;
    } else if (facilityType === 'schulsozialarbeit') {
      this.showSchulsozialarbeitMarkers = !this.showSchulsozialarbeitMarkers;
    } else if (facilityType === 'kindertageseinrichtungen') {
      this.showKindertageseinrichtungenMarkers = !this.showKindertageseinrichtungenMarkers;
    } else if (facilityType === 'schulen') {
      this.showSchulenMarkers = !this.showSchulenMarkers;
    }
    this.updateFacilities();
  }

  toggleButtonGroup() {
    this.showButtons = !this.showButtons;
  }
}
