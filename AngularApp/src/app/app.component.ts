import { Component } from '@angular/core';
import {  RouterLink, RouterOutlet } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';





@Component({
    selector: 'app-root',
    standalone: true,
    templateUrl: './app.component.html',
    styleUrl: './app.component.css',
    imports: [
        RouterOutlet,
        FormsModule,
        CommonModule,
        RouterLink,
        FontAwesomeModule,
        
    ]
})

export class AppComponent {
  title = 'AngularApp';
}
