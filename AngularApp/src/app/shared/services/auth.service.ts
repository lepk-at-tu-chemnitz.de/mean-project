import { HttpClient } from '@angular/common/http';
import { Injectable, inject } from '@angular/core';
import { apiUrls } from '../../api.urls';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  http = inject(HttpClient);

  private currentUserId: string | null = null;

  registerService(registerObj: any) {
    return this.http.post<any>(`${apiUrls.authServiceApi}register`, registerObj);
  }

  loginService(loginObj: any) {
    return this.http.post<any>(`${apiUrls.authServiceApi}login`, loginObj);
  }

  logout(logoutObj: any) {
    return this.http.post<any>(`${apiUrls.authServiceApi}logout`, logoutObj);
  }

  setCurrentUserId(userId: string) {
    this.currentUserId = userId;
    localStorage.setItem('currentUserId', userId);
    console.log('User ID set:', userId);
  }

  getCurrentUserId(): string | null {
    return this.currentUserId || localStorage.getItem('currentUserId');
  }

  isLoggedIn(): boolean {
    return !!this.getCurrentUserId();
  }
}
