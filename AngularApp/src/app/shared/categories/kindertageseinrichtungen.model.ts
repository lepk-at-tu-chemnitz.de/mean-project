export interface Kindertageseinrichtungen {
    X: number;
    Y: number;
    OBJECTID: number;
    ID: number;
    TRAEGER: string;
    BEZEICHNUNG: string;
    KURZBEZEICHNUNG: string;
    STRASSE: string;
    PLZ: number;
    ORT: string;
    TELEFON: string;
    EMAIL: string;
  }
  