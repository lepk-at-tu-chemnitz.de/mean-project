export class Schulsozialarbeit {
    _id!: string;
    X!: number;
    Y!: number;
    OBJECTID!: number;
    ID!: number;
    TRAEGER!: string;
    LEISTUNGEN!: string;
    STRASSE!: string;
    PLZ!: number;
    ORT!: string;
    TELEFON!: number;
    FAX!: string;
  }
  