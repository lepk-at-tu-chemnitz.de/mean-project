export interface Schulen {
  X: number;
  Y: number;
  OBJECTID: number;
  TYP: number;
  ART: string;
  STANDORTTYP: number;
  BEZEICHNUNG: string;
  KURZBEZEICHNUNG: string;
  STRASSE: string;
  PLZ: number;
  ORT: string;
  TELEFON: string;
  FAX: string;
  EMAIL: string;
  PROFILE: string;
  SPRACHEN: string;
  WWW: string;
  TRAEGER: string;
  TRAEGERTYP: number;
  BEZIRGNR: number;
  GEBIETSARTNUMMER: number;
  EditDate: Date;
}
