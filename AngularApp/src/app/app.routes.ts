import { Routes } from '@angular/router';
import { authGuard } from './shared/services/auth.guard';

export const routes: Routes = [
  { path: '', loadComponent: () => import('./pages/login/login.component') },
  { path: 'register', loadComponent: () => import('./pages/register/register.component') },
  { path: 'forget-password', loadComponent: () => import('./pages/forget-password/forget-password.component') },
  { path: 'home', loadComponent: () => import('./home/home.component'), canActivate: [authGuard] },
];
