const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const cookieParser = require('cookie-parser');

const { mongoose } = require('./db'); 
const jugendberufshilfensController = require('./controllers/jugendberufshilfensController');
const schulsozialarbeitController = require('./controllers/schulsozialarbeitController');
const kindertageseinrichtungenController = require('./controllers/kindertageseinrichtungenController');
const schulenController = require('./controllers/schulenController');
const roleRouter = require('./routers/role');
const authRouter = require('./routers/auth');
const userRouter = require('./routers/user');
const favoritesRouter = require('./routers/favorites');

const app = express();

// Middleware
app.use(bodyParser.json());
app.use(cors({ origin: 'http://localhost:4200', credentials: true })); 
app.use(cookieParser());

// Routes
app.use('/jugendberufshilfens', jugendberufshilfensController);
app.use('/schulsozialarbeit', schulsozialarbeitController);
app.use('/kindertageseinrichtungen', kindertageseinrichtungenController);
app.use('/schulen', schulenController);
app.use('/role', roleRouter);
app.use('/auth', authRouter);
app.use('/user', userRouter);
app.use('/favorites', favoritesRouter);

// Error Handler Middleware
app.use((err, req, res, next) => {
    const statusCode = err.status || 500;
    const errorMessage = err.message || "Something went wrong!";
    return res.status(statusCode).json({
        success: false,
        status: statusCode,
        message: errorMessage
    });
});

// Response Handler Middleware
app.use((obj, req, res, next) => {
    console.log("Middleware Response object:", obj);
    const statusCode = obj.status || 500;
    const message = obj.message || "Something went wrong!";
    const success = [200, 201, 204].includes(statusCode);

    return res.status(statusCode).json({
        success: success,
        status: statusCode,
        message: message,
        data: obj.data
    });
});

app.listen(3000, () => console.log('Server started at port : 3000'));
