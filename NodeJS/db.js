const mongoose = require('mongoose');
require('dotenv').config(); // Load environment variables

const connectToMongo = async () => {
    try {
        await mongoose.connect(process.env.MONGO_URL, { useNewUrlParser: true, useUnifiedTopology: true });
        console.log("Connected to MongoDB");
    } catch (error) {
        console.error("Failed to connect to MongoDB", error);
    }
};

connectToMongo();

module.exports =mongoose;