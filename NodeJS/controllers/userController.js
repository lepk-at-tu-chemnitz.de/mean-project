const User= require ('../models/User.js');
const { CreateSuccess } = require('../utils/success.js');
const { CreateError } = require('../utils/error.js');

const getAllUsers = async (req, res, next)=>{
    try{
        const users = await User.find();
        return next(CreateSuccess(200, users));
    }catch{
        return next(CreateError(500, 'Internal Server Error!'));
    }
}

const getUserById = async (req, res, next)=>{
    try{
        const user = await User.findById(req.params.id);
        if(!user){
            return next(CreateError(404, 'User not found!'));
        }else{
            return next(CreateSuccess(200,  user));
        }
    }catch{
        return next(CreateError(500, 'Internal Server Error!'));
    }
}


module.exports = {getAllUsers, getUserById}