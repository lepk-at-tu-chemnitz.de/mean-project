const express = require('express');
var router = express.Router();
var { Types: { ObjectId } } = require('mongoose');

var { Kindertageseinrichtungen } = require('../models/kindertageseinrichtungen');

router.get('/', async (req, res) => {
    try {
        const kindertageseinrichtungen = await Kindertageseinrichtungen.find();
        res.send(kindertageseinrichtungen);
    } catch (err) {
        console.error('Error in Retrieving Data:', err);
        res.status(500).send('Error retrieving data');
    }
});

// GET records by Id
router.get('/:id', async (req, res) => {
    if (!ObjectId.isValid(req.params.id)) {
        return res.status(400).send(`No record with given id: ${req.params.id}`);
    }
    try {
        const kindertageseinrichtungen = await Kindertageseinrichtungen.findById(req.params.id);
        if (!kindertageseinrichtungen) {
            return res.status(404).send('Id not found');
        }
        res.send(kindertageseinrichtungen);
    } catch (err) {
        console.error('Error in Retrieving Data by Id:', err);
        res.status(500).send('Error retrieving data by id');
    }
});

module.exports = router;
