const Role = require ('../models/Role.js');
const User= require ('../models/User.js');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { CreateSuccess } = require('../utils/success.js');
const { CreateError } = require('../utils/error.js');


const register = async (req, res, next) => {
    try {
        // Find the role 'User'
        const role = await Role.findOne({ role: 'User' });
        if (!role) {
            return next(CreateError(400, 'User role not found'));
        }

        // Generate salt and hash the password
        const salt = await bcrypt.genSalt(10);
        const hashedPassword = await bcrypt.hash(req.body.password, salt);

        // Create a new user
        const newUser = new User({
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            username: req.body.username,
            email: req.body.email,
            password: hashedPassword,
            roles: [role._id],  // Assuming roles is an array of ObjectId
        });

        // Save the new user
        await newUser.save();
        return next(CreateSuccess(200, 'User Registered Succesfully!'));
    } catch (error) {
        return next(CreateError(500, 'Internal Server Error!'));
    }
}

const registerAdmin = async (req, res, next) => {
    try {
        // Find the role 'User'
        const role = await Role.findOne({});
        if (!role) {
            return next(CreateError(400, 'Admin role not found'));
        }

        // Generate salt and hash the password
        const salt = await bcrypt.genSalt(10);
        const hashedPassword = await bcrypt.hash(req.body.password, salt);

        // Create a new user
        const newUser = new User({
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            username: req.body.username,
            email: req.body.email,
            password: hashedPassword,
            isAdmin: true,
            roles:[role._id], 
        });

        await newUser.save();
        return next(CreateSuccess(200, 'Admin User Registered Succesfully!'));
    } catch (error) {
        return next(CreateError(500, 'Internal Server Error!'));
    }
}

const login = async (req, res, next) => {
    try {
        const { email, password } = req.body;
        if (!email || !password) {
            return next(CreateError(400, 'Email and password are required!'));
        }
        const user = await User.findOne({ email: email }).populate("roles", "role");
        if (!user) {
            return next(CreateError(400, 'User not found'));
        }
        
        const isPasswordCorrect = await bcrypt.compare(password, user.password);
        if (!isPasswordCorrect) {
            return next(CreateError(400, 'Invalid Password!'));
        }

        const token = jwt.sign(
            { id: user._id, isAdmin: user.isAdmin, roles: user.roles.map(role => role.role) },
            process.env.JWT_SECRET
        );
        res.cookie("access_token", token, { httpOnly: true })
            .status(200)
            .json({
                status: 200,
                message: 'Login Successful',
                data: user
            });
    } catch (error) {
        console.error('Login Error:', error);
        return next(CreateError(500, 'Internal Server Error!'));
    }
};



module.exports = {register, login, registerAdmin}