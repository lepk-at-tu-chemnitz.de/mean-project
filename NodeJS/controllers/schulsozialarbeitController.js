const express = require('express');
var router = express.Router();
var { Types: { ObjectId } } = require('mongoose');

var { Schulsozialarbeit } = require('../models/schulsozialarbeit');

router.get('/', async (req, res) => {
    try {
        const schulsozialarbeit = await Schulsozialarbeit.find();
        res.send(schulsozialarbeit);
    } catch (err) {
        console.log('Error in Retrieving Data :' + JSON.stringify(err, undefined, 2));
        res.status(500).send('Error retrieving data');
    }
});

// GET records by Id
router.get('/:id', async (req, res) => {
    if (!ObjectId.isValid(req.params.id)) {
        return res.status(400).send(`No record with given id: ${req.params.id}`);
    }
    try {
        const schulsozialarbeit = await Schulsozialarbeit.findById(req.params.id);
        if (!schulsozialarbeit) {
            return res.status(404).send('Id not found');
        }
        res.send(schulsozialarbeit);
    } catch (err) {
        console.log('Error in Retrieving Data by Id :' + JSON.stringify(err, undefined, 2));
        res.status(500).send('Error retrieving data by id');
    }
});

module.exports = router;
