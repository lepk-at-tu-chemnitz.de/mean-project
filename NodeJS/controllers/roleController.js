const Role = require ('../models/Role.js');
const { CreateSuccess } = require('../utils/success.js');
const { CreateError } = require('../utils/error.js');

//Create a new role in DB
const createRole = async (req, res, next)=>{
    try{
        if(req.body.role && req.body.role !== ''){
            const newRole = new Role(req.body);
            await newRole.save();
            return next(CreateSuccess(200, 'Role Created Succesfully!'));
        }else{
            return next(CreateError(400, 'Role is required'));
        }
    }catch(error){
        return next(CreateError(500, 'Internal Server Error!'));
    }
}
//Update role in DB
const updateRole = async (req, res, next)=>{
    try{
        const role = await Role.findById({_id: req.params.id});
        if(role){
            const newData = await Role.findByIdAndUpdate(
                req.params.id,
                {$set: req.body},
                {new: true}
            );
            return res.status(200).send("Role Updated");
        }else{  
            return next(CreateError(400, 'Role not found'));
        }
    }catch{
        return next(CreateError(500, 'Internal Server Error!'));
    }

}
//Get all roles in DB
const getRole = async (req, res, next)=>{
    try{
        const roles = await Role.find();
        return next(CreateSuccess(200, roles));
    }catch{
        return next(CreateError(500, 'Internal Server Error!'));
    }
}
//Delete role in DB
const deleteRole =async (req, res, next)=>{
    try{
        const role = await Role.findById({_id: req.params.id});
        if(role){
            await Role.findByIdAndDelete(req.params.id);
            return next(CreateSuccess(200, 'Role Deleted Succesfully'));
            }else{
                return next(CreateError(400, 'Role not found'));
            }
        }catch{
            return next(CreateError(500, 'Internal Server Error!'));
    }
}

module.exports = {createRole , updateRole, getRole, deleteRole }