const express = require('express');
const router = express.Router();
const { Schulen } = require('../models/schulen');

// Get all records
router.get('/', async (req, res) => {
    try {
        const schulen = await Schulen.find();
        res.send(schulen);
    } catch (err) {
        console.error('Error in Retrieving Data :' + JSON.stringify(err, undefined, 2));
        res.status(500).send('Error retrieving data');
    }
});

// Get record by ID
router.get('/:id', async (req, res) => {
    if (!ObjectId.isValid(req.params.id)) {
        return res.status(400).send(`No record with given id: ${req.params.id}`);
    }
    try {
        const schulen = await Schulen.findById(req.params.id);
        if (!schulen) {
            return res.status(404).send('Id not found');
        }
        res.send(schulen);
    } catch (err) {
        console.error('Error in Retrieving Data by Id :' + JSON.stringify(err, undefined, 2));
        res.status(500).send('Error retrieving data by id');
    }
});

module.exports = router;
