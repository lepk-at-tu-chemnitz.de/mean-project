const jwt = require('jsonwebtoken');
const { CreateError } = require('./error');

const verifyToken = async (req, res, next)=>{
    try{
        const token = req.cookies.access_token;
        if(!token)
            return next(CreateError(401, 'You are not authenticated!'));
            jwt.verify(token, process.env.JWT_SECRET, (err, user)=>{
            if(err) {
                return next(CreateError(403, 'Token is not valid!'));
            }
            req.user = user;
            next();
        })
    }catch{
        return next (CreateError(401,'No token provided.'))
    }
}

const verifyUser = async (req, res, next)=>{ 
        verifyToken(req, res, () =>{ 
            if(req.user.id === req.params.id || req.user.isAdmin){
                next();
            }else{
                return next(CreateError(403, 'You are not authorized!'))
            }
        })   
}

const verifyAdmin= async (req, res, next)=>{ 
    verifyToken(req, res, ()=>{
        if(req.user.isAdmin){
            next();
        }else{
            return next(CreateError(403, 'You are not authorized!'))
        }
    })
}


module.exports = {verifyToken, verifyUser, verifyAdmin}