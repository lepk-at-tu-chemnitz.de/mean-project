const mongoose = require('mongoose');

const schulenSchema = new mongoose.Schema({
  X: { type: Number, required: true },
  Y: { type: Number, required: true },
  OBJECTID: { type: Number, required: true },
  TYP: { type: Number, required: true },
  ART: { type: String, required: true },
  STANDORTTYP: { type: Number, required: true },
  BEZEICHNUNG: { type: String, required: true },
  KURZBEZEICHNUNG: { type: String, required: true },
  STRASSE: { type: String, required: true },
  PLZ: { type: Number, required: true },
  ORT: { type: String, required: true },
  TELEFON: { type: String, required: true },
  FAX: { type: String, required: true },
  EMAIL: { type: String, required: true },
  PROFILE: { type: String, required: true },
  SPRACHEN: { type: String, required: true },
  WWW: { type: String, required: true },
  TRAEGER: { type: String, required: true },
  TRAEGERTYP: { type: Number, required: true },
  BEZIRGNR: { type: Number, required: true },
  GEBIETSARTNUMMER: { type: Number, required: true },
  EditDate: { type: Date, required: true }
}, { collection: 'schulen' });

const Schulen = mongoose.model('Schulen', schulenSchema);

module.exports = { Schulen };
