const mongoose = require('mongoose');
const { Schema } = mongoose;

const UserSchema = new Schema(
    {
        firstName: {
            type: String,
            required: true
        },
        lastName: {
            type: String,
            required: true
        },
        username: {
            type: String,
            required: true,
            unique: true
        },
        email: {
            type: String,
            unique: true,
            required: true
        },
        password: {
            type: String,
            required: true
        },
        profileImage: {
            type: String,
            default: "../assets/default.jpg",
            required: false
        },
        isAdmin: {
            type: Boolean,
            default: false
        },
        roles: {
            type: [Schema.Types.ObjectId],
            required: false,
            ref: 'Role'
        },
        favorites: {
            type: [{ type: Schema.Types.ObjectId, ref: 'Facility' }],
            required: false
        }
    },
    {
        timestamps: true
    }
);

const User = mongoose.models.User || mongoose.model('User', UserSchema);

module.exports = User;
