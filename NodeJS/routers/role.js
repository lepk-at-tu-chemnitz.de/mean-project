const express = require('express');
const { updateRole, createRole, getRole, deleteRole } = require('../controllers/roleController.js');
const { verifyAdmin } = require('../utils/verifyToken.js');

const router = express.Router();

//Create a new role in DB
router.post('/create',verifyAdmin, createRole);
//Update role in DB
router.put('/update/:id',verifyAdmin, updateRole);
//Get all roles in DB
router.get('/get', getRole);
//Delete role in DB
router.delete('/delete/:id', deleteRole)

module.exports = router;