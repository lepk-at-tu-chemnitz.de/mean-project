const express = require('express');
const router = express.Router();
const { verifyUser } = require('../utils/verifyToken');
const Favorite = require('../models/favorite'); 


router.post('/:userId/:facilityId', verifyUser, async (req, res) => {
  try {
    const { userId, facilityId } = req.params;

    console.log('Incoming request:', { userId, facilityId }); 

    const existingFavorite = await Favorite.findOne({ userId, facilityId });
    if (existingFavorite) {
      return res.status(400).json({ success: false, message: 'Facility already in favorites' });
    }

    const newFavorite = new Favorite({ userId, facilityId });
    await newFavorite.save();

    res.status(200).json({ success: true, message: 'Facility added to favorites' });
  } catch (error) {
    console.error('Error adding to favorites:', error);
    res.status(500).json({ success: false, message: 'Internal Server Error' });
  }
});

router.get('/:userId', verifyUser, async (req, res) => {
  try {
    const { userId } = req.params;
    const favorites = await Favorite.find({ userId }).populate('facilityId');

    res.status(200).json({ success: true, data: favorites });
  } catch (error) {
    console.error('Error getting favorites:', error);
    res.status(500).json({ success: false, message: 'Internal Server Error' });
  }
});

module.exports = router;
