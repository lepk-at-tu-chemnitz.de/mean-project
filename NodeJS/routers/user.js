const express = require('express');
const { getAllUsers, getUserById } = require('../controllers/userController.js');
const { verifyUser, verifyAdmin } = require('../utils/verifyToken.js');
const router = express.Router();

//get all
router.get('/', verifyAdmin, getAllUsers);
//get by id
router.get('/:id', verifyUser, getUserById);

module.exports = router;