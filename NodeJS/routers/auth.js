const express = require('express');
const { register, login, registerAdmin } = require('../controllers/authController.js');
const router = express.Router();

//register
router.post('/register', register)
//login
router.post('/login', login)
//register as Admin
router.post('/register-admin', registerAdmin)

module.exports = router;